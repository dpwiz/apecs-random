# apecs-random

Because friends don't let friends use `randomRIO`.

## Install

Add `mwc-probability` and `apecs-random` to your deps.

## Usage

Add component:

```haskell
import qualified Apecs.Components.Random

makeWorld
  [ ''Random.RandomGen
  -- ...
  ]
```

Initialize:

```haskell
import qualified Apecs.System.Random as Random

main :: IO ()
main = do
  w <- initWorld
  runWith w $ do
    Random.initialize
    -- ...
```

Flip a coin:

```haskell
import qualified Apecs.System.Random as Random
import qualified System.Random.MWC.Probability as Prob

fairCoin :: SystemT World IO Bool
fairCoin = Random.sample $ Prob.bernoulli 0.5
```
